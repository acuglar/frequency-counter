//atribui a #countButton function frequencia de letras e palavras no texto
document.getElementById("countButton").onclick = function (e) {

    e.preventDefault()
    
    document.getElementById("lettersDiv").innerText = "";  //zerar output anterior onclick;
    document.getElementById("wordsDiv").innerText = "";

    //atribui a typedText o text inserido
    let typedText = document.getElementById("textInput").value;
    //letras para minúsculas
    typedText = typedText.toLowerCase();
    //ignora caracteres que não letras comuns, espaços e apóstrofos. 
    typedText = typedText.replace(/[^a-z'\s]+/g, "");

    

    //CONTADOR LETRAS//
    const letterCounts = {};
    //ao percorrer typedText, se primeira ocorrência de currentLetter set 1, else add 1
    for (let i = 0; i < typedText.length; i++) {
        let currentLetter = typedText[i];

        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    }
    console.log(letterCounts)  //Objeto para trabalhar com índices associativos 

    //display output em lettersDiv    
    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }

    //CONTADOR PALAVRAS//
    const wordCounts = {};
    let word = typedText.split(/\s/);

    for (let i = 0; i < word.length; i++) {
        let currentWord = word[i];

        if (wordCounts[currentWord] === undefined) {
            wordCounts[currentWord] = 1;
        } else {
            wordCounts[currentWord]++;
        }
    }
    console.log(word)
    console.log(wordCounts)

    for (let word in wordCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
        
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }
    


}